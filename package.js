Package.describe({
  name: 'ziedmahdi:moment-timezone',
  version: '0.5.2',
  // Brief, one-line summary of the package.
  summary: 'Timezone support for moment.js, packaged for Meteor (only Server Side)',
  // URL to the Git repository containing the source code for this package.
  git: 'https://bitbucket.org/ziedmahdi/moment-timezone',
  documentation: null
});

Package.onUse(function(api) {
  api.versionsFrom('1.2.1');
  api.use('momentjs:moment@2.6.0');
  api.imply('momentjs:moment');

  api.addFiles([
    'moment-timezone-with-data.js'
  ], ['server']);

  api.addFiles([
      'jstz.js'
  ], ['client']);

  api.export('jstz', ['client']);
});

Package.onTest(function(api) {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('ziedmahdi:moment-timezone');
  api.addFiles('moment-time-zone-tests.js');
});
